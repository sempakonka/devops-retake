#!/bin/bash
echo "Waiting for backend to start..."
# Use wait-for-it.sh to wait for the backend service to start up
/app/wait-for-it.sh backend:5000 --timeout=0 --strict -- echo "Backend is up"

# Initialize variables
name=""
email=""
shortname=""

# Read the file
while read -r line; do
  # Check if line contains a name
  if [[ $line == *"name"* ]]; then
    # Extract the name and remove unnecessary characters
    name="${line//name{/}"
    name="${name//\}/}"
    name="${name//\"/}"
    name="${name//[[:blank:]]/}"
  fi

  # Check if line contains an email
  if [[ $line == *"email"* ]]; then
    # Extract the email and remove unnecessary characters
    email="${line//email{/}"
    email="${email//\}/}"
    email="${email//\"/}"
    email="${email//[[:blank:]]/}"
  fi

  # Check if line contains a code
  if [[ $line == *"code"* ]]; then
    # Extract the code and remove unnecessary characters
    shortname="${line//code{/}"
    shortname="${shortname//\}/}"
    shortname="${shortname//\"/}"
    shortname="${shortname//[[:blank:]]/}"
  fi

  # If line contains 'person', POST the current data and clear it for the next person
  if [[ $line == *"person"* ]]; then
    # Post the data only if all fields are filled
    if [[ -n $name && -n $email && -n $shortname ]]; then
      curl --header "Content-Type: application/json" --request POST --data "{ \"email\": \"$email\", \"shortname\": \"$shortname\", \"fullname\": \"$name\" }" http://backend:5000
      # Clear the variables for the next person
      name=""
      email=""
      shortname=""
    fi
  fi
done < "data.txt"

# Post the last set of data if it exists
if [[ -n $name && -n $email && -n $shortname ]]; then
  curl --header "Content-Type: application/json" --request POST --data "{ \"email\": \"$email\", \"shortname\": \"$shortname\", \"fullname\": \"$name\" }" http://backend:5000
fi
