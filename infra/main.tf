provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"

  // this is for the postgres db
  enable_dns_hostnames = true
  enable_dns_support = true
  // ^^^^^^^^^

  tags = {
    Name = "my-vpc"
  }
}

resource "aws_subnet" "my_subnet" {
  for_each = var.subnets

  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = each.value.cidr_block
  availability_zone       = each.value.availability_zone
  map_public_ip_on_launch = true
  tags = {
    Name = "my-subnet-${each.key}"
  }
}

resource "aws_internet_gateway" "my_gateway" {
  vpc_id = aws_vpc.my_vpc.id
  tags = {
    Name = "my-gateway"
  }
}

resource "aws_route_table" "my_route_table" {
  vpc_id = aws_vpc.my_vpc.id
  tags = {
    Name = "my-route-table"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_gateway.id
  }
}

resource "aws_route_table_association" "my_route_table_assoc" {
  for_each = aws_subnet.my_subnet

  subnet_id      = each.value.id
  route_table_id = aws_route_table.my_route_table.id
}


data "aws_ami" "ubuntu_ami" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_security_group" "my_security_group" {
  name        = "my-security-group"
  description = "Allow HTTP and SSH traffic"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }



  ingress {
    from_port   = 80
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "my-security-group"
  }
}





resource "aws_lb" "my_alb" {
  name               = "my-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.my_security_group.id]
  subnets            = [for s in aws_subnet.my_subnet : s.id]

  tags = {
    Name = "my-alb"
  }
}

resource "aws_lb_target_group" "my_target_group" {
  name     = "my-target-group"
  port     = 5000
  protocol = "HTTP"
  vpc_id   = aws_vpc.my_vpc.id


  health_check {
    interval            = 120
    path                = "/"
    timeout             = 25
    healthy_threshold   = 3
    unhealthy_threshold = 4
    protocol            = "HTTP"
    port                = "traffic-port"
  }


  tags = {
    Name = "my-target-group"
  }
}

resource "aws_lb_listener" "my_listener" {
  load_balancer_arn = aws_lb.my_alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my_target_group.arn
  }
}


resource "aws_instance" "my_instance_1" {
  ami           = data.aws_ami.ubuntu_ami.id
  instance_type = "t2.micro"
  key_name      = "sem key"
  vpc_security_group_ids = [aws_security_group.my_security_group.id]

  user_data = base64encode(<<-EOF
    #!/bin/bash
    sudo apt update -y
    sudo apt install -y docker.io
    sudo usermod -aG docker $USER
    newgrp docker
  EOF
  )

  subnet_id = tolist(values(aws_subnet.my_subnet))[0].id
  tags = {
    Name = "my-instance-1"
  }
}

resource "aws_instance" "my_instance_2" {
  ami           = data.aws_ami.ubuntu_ami.id
  instance_type = "t2.micro"
  key_name      = "sem key"
  vpc_security_group_ids = [aws_security_group.my_security_group.id]

  user_data = base64encode(<<-EOF
    #!/bin/bash
    sudo apt update -y
    sudo apt install -y docker.io
    sudo usermod -aG docker $USER
    newgrp docker
  EOF
    )

  subnet_id = tolist(values(aws_subnet.my_subnet))[1].id
  tags = {
    Name = "my-instance-2"
  }
}


resource "aws_instance" "my_instance_3" {
  ami           = data.aws_ami.ubuntu_ami.id
  instance_type = "t2.micro"
  key_name      = "sem key"
  vpc_security_group_ids = [aws_security_group.my_security_group.id]

  user_data = base64encode(<<-EOF
    #!/bin/bash
    sudo apt update -y
    sudo apt install -y docker.io
    sudo usermod -aG docker $USER
    newgrp docker
  EOF
  )

  subnet_id = tolist(values(aws_subnet.my_subnet))[1].id
  tags = {
    Name = "my-instance-3"
  }
}



resource "aws_lb_target_group_attachment" "attach_instance_1" {
  target_group_arn = aws_lb_target_group.my_target_group.arn
  target_id        = aws_instance.my_instance_1.id
}

resource "aws_lb_target_group_attachment" "attach_instance_2" {
  target_group_arn = aws_lb_target_group.my_target_group.arn
  target_id        = aws_instance.my_instance_2.id
}


resource "aws_db_instance" "default" {
  allocated_storage    = 20
  engine               = "postgres"
  engine_version       = "15.3"
  instance_class       = "db.t3.micro"
  db_name              = "DevOps"
  username             = "sempakonka"
  password             = "Hello2023"
  parameter_group_name = "default.postgres15"
  skip_final_snapshot  = true
  publicly_accessible  = true
  vpc_security_group_ids = [aws_security_group.my_security_group.id]
  db_subnet_group_name = aws_db_subnet_group.my_db_subnet_group.name
  multi_az = true
}


// for the high available db
resource "aws_db_subnet_group" "my_db_subnet_group" {
  name       = "my-db-subnet-group"
  subnet_ids = [for s in aws_subnet.my_subnet : s.id]

  tags = {
    Name = "my-db-subnet-group"
  }
}

